package com.garneau.java.web.util;

public class GeoDistance {
	
	/* distance entre deux tuples de géocoordonnées */
    private double distance(double lat1, double lon1, double lat2, double lon2) {
		double theta = lon1 - lon2;
		double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
		dist = Math.acos(dist);
		dist = rad2deg(dist);
		dist = dist * 60 * 1.1515;

		/* Valeur de distance en kms */
		dist = dist * 1.609344;

		return (dist);
	}

	/* Conversion degrés décimaux en radians */
	private double deg2rad(double deg) {
		return (deg * Math.PI / 180.0);
	}

	/* Conversion radians en degré décimaux */
	private double rad2deg(double rad) {
		return (rad * 180 / Math.PI);
	}
	
	/* distance entre deux tuples de géocoordonnées (String) */
	public double compareDistance(String adresse1, String adresse2) {
		String[] ltlg1 = adresse1.split(",|, ");
		String[] ltlg2 = adresse2.split(",|, ");
		
		return distance(Double.parseDouble(ltlg1[0]), Double.parseDouble(ltlg1[1]), Double.parseDouble(ltlg2[0]), Double.parseDouble(ltlg2[1]));
	}

}
