<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Section Membre</title>
</head>
<body>

<h1>Bienvenue <c:out value="${ATT_SESSION_USER.nomUtilisateur}"/></h1>
	
	<p>Votre groupe : <c:out value="${ATT_SESSION_USER.groupe}"/></p>
	
	<c:if test="${!empty sessionScope.ATT_SESSION_USER}">
                    <p class="succes">Vous êtes connecté(e) avec l'adresse : ${sessionScope.ATT_SESSION_USER.email}</p>
    </c:if>
    
    <h2><a href="${pageContext.request.contextPath}/membre/panier" >Accéder à votre panier</a></h2>

</body>
</html>