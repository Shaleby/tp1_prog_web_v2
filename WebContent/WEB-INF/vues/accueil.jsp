<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Accueil</title>
</head>
<body>

<h1>Site de Gestion de Coops</h1>

<!--  <p>Vous n'êtes pas membre ? <a href="<c:url value="/Inscription.jsp"/>">Inscrivez vous !</a></p> -->
<p>Vous n'êtes pas membre ? <a href="${pageContext.request.contextPath}/inscription">Inscrivez vous !</a></p>

<p>Déjà membre ? <a href="${pageContext.request.contextPath}/connexion">Connectez-vous.</a></p>

</body>
</html>